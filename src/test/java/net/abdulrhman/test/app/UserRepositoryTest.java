package net.abdulrhman.test.app;

import static org.junit.Assert.assertNotNull;

import java.util.Date;

import net.abdulrhman.backend.entities.User;
import net.abdulrhman.backend.repositories.UserRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:META-INF/application-context.xml")
public class UserRepositoryTest {

	@Autowired
    UserRepository repository;
	
	@Test
	public void addingUserTest() {
		User user = new User();
		
		user.setFirstName("Abdulrhman");
		user.setLastName("Alkhodiry");
		user.setEmail("zeroows@gmail.com");
		user.setPhoneNumber("966533");
		user.setUsername("zeroows");
		user.setDateOfBirth(new Date());
		
		repository.save(user);
		
		User dbInmate = repository.findOne(user.getId());
		
		assertNotNull(dbInmate);
		System.out.println(String.format("%s, %s", dbInmate.getLastName(), dbInmate.getFirstName()));
		System.out.println(String.format("Date of birth: %s", dbInmate.getDateOfBirth()));
	}

}
