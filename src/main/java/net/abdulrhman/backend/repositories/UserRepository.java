package net.abdulrhman.backend.repositories;

import net.abdulrhman.backend.entities.User;

import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<User, Integer> {

}
